# Journey of The Mediocre Programmer
## How we got here

You have your own unique story of how you got here as a programmer. You might have found out about programming as a curious child who wanted to see what the computer could do. Or you could have arrived as an adult who heard about these things called computers that you could program. Whatever the case, you had a journey to get to this point, and you learned a certain amount to get here. You spent your free time learning how to code, or you were fortunate to be able to work on programming as part of your job. You went to school to learn more about programming or you took training classes. You bought books or read articles online to learn more about programming. Whatever path you took you began your journey as a programmer.

And now you feel stuck.

You look around and wonder if you'll ever know everything that you should know. You read an article on a site and your interest is piqued. An online friend mentions this neat thing that they've found and expects that you will learn more about it. Your colleague found something that might solve an issue you're having on a project and now you have one more thing to learn. 

New topics and technologies seem to emerge almost weekly. These "things" creep into our programming discussions and in our work. Perhaps you're finding these new things appearing in job postings requiring a minimum of 3+ years of experience, and you're wondering how could anyone have that level of experience. Perhaps you chose to ignore these things for a while and now they've become a driving factor in your work. It's as if someone flipped a bit and now you're unworthy of being called a programmer unless you were an early adopter of these things.

Each of these experiences causes you to feel as though you are incomplete without learning these new things. They show us that no matter what our current experience is there are still gaps in our knowledge that must be filled. As you look to the horizon you can see the gaps that creep up in between where you are and where you think you should be.

## The Gap

I've chosen the word "gap" to describe the difference between where you are now and where you think you should be for good reason. A gap is something imposed by others, whether by force or by neglect. If a gap appears in your garden fence it means the fence is weaker at protecting the garden. A gap can also be something that requires our attention. "Mind the gap" is a phrase coined in the late 1960s by the London Underground to warn folks of the space between the platform and the train cars. If people aren't careful around that gap it could lead to an unsafe situation.

The gap in this case is the distance between our current abilities and where we think we should be. Sometimes the gaps are self-imposed because of our desires to improve ourselves, but more often the gap is externally imposed. 

One of the biggest creators of gaps in our programming career is change. As programmers we are fully aware of the cycle of change in programming culture. We're constantly having changes thrown at us: changing technology, changing priorities at work, or even changing our strategies to try to keep up with the demands made of us.

Change can also come from within our community. Our community of programmers and users could move on to new positions and new technologies. We may no longer get the support we need to do our jobs and be faced with the prospect that we too need to update our skills or be left behind in an abandoned community.

Change can lead to stress. Stress is prevalent in programmer circles because things are often changing. What worked on Friday afternoon can be broken on Monday morning because of a change to a library that we were using. Our development environment could break because of an upgrade that didn't apply properly. Production might need security fixes, and those fixes mean we have to redo a major piece of our software because it no longer works. There are plenty of ways in which we are kept in a cycle of change.

Not all change is bad. The software that we use could have very good reasons for changing. New features might be introduced that require new ways of thinking about your code. Security fixes may mean that our systems are more resilient to outside attacks, but require different ways of using that system. New and better optimizations may lead to our code running faster but needing a few tweaks to take advantage of those speed benefits. A refactor of an API can lead to cleaner and more concise ways of interacting with another system, but introduce changes that are not backward-compatible with current code. 

Change can be positive, but change requires time and effort in order to adapt to it. The gap can only be closed if we have the resources and time to work on it. If we're struggling with our current workload and someone changes how something works we now have to budget our time to adapt to that change. If our mental-muscle-memory knew how something worked and is now faced with a significant change it requires us to re-train that mental-muscle-memory to match how it now works. And if you're already feeling like you don't understand the systems and environments that you're working on then adding additional change can leave you feeling stranded among the newly-formed gaps in your knowledge.

## Closing The Gap

I'd love to tell you that there's a way to close the gap; a way to say you've learned it all and can feel confident that you have mastered the totality of programming.

Sadly, I haven't found a way to close all of the gaps.

You can keep learning everything there is to know about whatever topic you've chosen to learn. You can take every course available. You can attend every talk and colloquium, read papers about the subject, and even do your own research, and you can still feel like you haven't closed the gap.

So if there's no way to completely close the gap what can you do?

There are three options available to you:

The first option is not to try. Realize there will always be more to learn. Why bother? It's easier to tell yourself you'll never be able to close the gaps in your knowledge. The best option, you tell yourself, is to stick with what you know and ride that out as long as you can. 

The second option is to try to do everything at once. You grab every book, blog post, paper, video, or what-have-you to try to learn the topic. Next, you realize that you only have a finite amount of time to learn the topic, and that you can't use all of that material at once. You look over your progress and despair that your learning is not progressing as quickly as you would like. You blame the materials for your lack of progress and look for something else that will help you better learn the topic. Frustration sets in as you blame yourself for not starting earlier to close this gap.

The third option is the more measured approach. You start small with small tasks and work your way toward the goal. Rather than look at the gap as something to be closed, you realize that you can't know the totality of whatever topic you're learning. You enjoy the knowledge you receive in the pursuit of learning the topic. You keep a steady pace toward learning as much as you can. Instead of lamenting that you didn't start sooner you're grateful that you started at all.

Of these three options the first and third are the ones where you'll find the most contentment. The first option (not trying) allows you to sit with the knowledge you have. But there's a downside to just staying in place. Our industry is constantly changing and technology continues to move. What used to be the norm becomes legacy and what was just around the corner becomes the thing that is in demand.

One of the most useful skills of a programmer is the ability to adapt to new technology. As our technological environment changes, our ability to adapt to those changes allows us to continue on as programmers. Faster machines, different technologies, different devices, different requirements; each of these brings us exciting challenges if we recognize them. But they also take time to learn and create gaps in our knowledge. Relying on our previous knowledge to carry us through these changes isn't going to be enough. We're challenged to adapt to the new surroundings. 

The second option (trying to cram and becoming frustrated) is the least optimal path. Trying to learn by grabbing every available resource and jamming it into our brains is a path to frustration, fatigue, and burnout. Many developers try this because they feel the need to adapt to the new environment, but it's difficult to make sweeping changes all at once. It's like trying to evolve wings because you're late for an appointment: you'll be frustrated with your inability to grow wings and still be late for your appointment. The second option also measures your progress by how much further you feel you need to go. It discounts the progress you've made, and creates an endless cycle of running toward a moving finish line.

Of the three options it's the third option that makes the most sense. Taking a measured approach in closing the gaps of our knowledge allows us more joy in our learning process. By breaking down each of the steps on our journey we give ourselves little wins along the way. Instead of expecting a grand transformation we allow ourselves gradual changes and mutations to adapt to our environment.

With this measured and gradual approach we also gain the wisdom that we don't have to close all of the gaps. We can allow ourselves to keep learning in the areas that we need to and gradually build up our skills. 

We also can realize that closing the gap is an illusion. As long as we're alive there will always be gaps; new things are created every day. We can choose how far we want to go to become more expert in whatever topic we chose to learn. We can still strive to learn as much as we can, but we do so with a sense of joy in the learning process, not from some perverse need to try to collect the totality of computing knowledge into our heads.

## The Journey is the Reward

The secret to moving forward in our journey as programmers and developers is realizing that each step we take along that path is valuable and worthy of our attention. Just because we haven't learned the latest technology in a fortnight doesn't mean we should give up trying. Nor does learning a technology only to watch it become overshadowed by something else mean that our learning time was wasted. Every challenge we face, every technology we learn, and every hour we spend coding is another step on our journey to becoming better programmers. Each mistake and wrong turn introduces us to opportunities to learn from those mistakes and grow as programmers and developers. There is no perfect path toward becoming better at this. Even if there were I'm sure we could point to any point in our past and say that it was less than perfect. Carving the perfect path from where we are to where we wish to be is not possible. Worse, it is an illusion that keeps us from moving forward in our daily practice of programming and developing. It may seem trite to say "the journey is the reward" but every day we work as a programmer and developer is one day closer to shoring up those gaps in our knowledge and becoming more content with how our skills are growing.
