# Introduction {-}

## The Mediocre Programmer?

Let's face it: we don't want to be mediocre programmers. We want to be ``[great|amazing|superlative]`` programmers! We want to be the programmers they call whenever they're in a bind, and we want to be the programmers that rush into unfamiliar code bases and produce perfect code in a matter of minutes. Code that would sit in the Louvre as a work of art, studied by generations of programmers for its intrinsic beauty and exceptional functionality.

Why would we want to be mediocre programmers? Isn't mediocre the opposite of great? Shouldn't we strive to be great programmers instead?

Sure, we should strive to be great programmers in the long term, but to become great programmers we have to pass through the mediocre programmer stage first.

Mediocre programmers know they're not great programmers (yet). Mediocre programmers see the distance between where they are and the greatness they want in their programming careers. They see the work that goes into a being great programmers and believe that if they do the work they too will become great programmers.

But they also see their own faults and failings. They see their browser history littered with online-searches for basic syntax and concepts. They see their email archives of questions they've asked other programmers. They wince at their code from several months ago and wonder if they'll ever get to be great programmers with all of these mistakes and missteps. They see the gap between them and great programmers, and it feels like the gap widens every step of the way. 

The mediocre programmer wonders if it's even worth it. They wonder if they should do something else with their lives other than computer programming. Maybe they're not as good as they thought they were, or maybe they lack that special talent that great programmers have. Maybe they feel they learned the wrong things early on in their journeys, or maybe they think they should have started sooner.

They see others being wildly successful and wonder if they were absent the day the great programmer genes were handed out.

The truth is we're all mediocre programmers in some way. We all still ask questions and have to look up syntax and concepts in our day-to-day programming. Computers continue to evolve and programmers keep adding complexity to everyday programming tasks. It takes a lot of mental bandwidth to keep all of those concepts fresh in our mind.

## Why this book?

This book is about helping you on your journey as a mediocre programmer. Together we'll uncover some of common misconceptions we have about programming, failure, and growth. We'll understand that the act of programming and development is something we undertake every day. Every day we can improve in small ways. It's these small changes that transform us from being mediocre programmers into better programmers.

There are plenty of books on how to become a better programmer out there. They tend to have checklists and other advice that the author deems important enough for you to do in order to become a better programmer. They tend to focus on specific improvements like choosing a better editor, writing better test cases, or drinking lots of water. Those books have lots of useful advice, but they read like a laundry list of things that you must do all at once in order to succeed. This book will try not to saddle you with more work (you likely have enough as it is). Rather, we'll discuss what it feels like to be a programmer. We'll talk about the emotions that show up while we're programming; the feelings of frustration, guilt, anger, and inadequacy. We'll cover the struggles in learning new things and keeping your skills current. We'll talk about those times when you feel like giving up and walking away from computing and whether those feelings come from a place of love or a worry that you're not keeping up.

This book is a personal journey for both of us. It's a memoir of my time as a programmer and my feelings along the way. I've thought many times about giving up and finding a different career path but doing anything other than being a computer programmer scares me even more. Does that mean I'm stuck in a perverse Ouroboros of self-pity and self-doubt? Hardly. It means that I need to dig deeper to understand why I chose the path of being a programmer and realize that it took a lot to get here and it's going to take a lot more to get where I want to be. It's a commitment to seeing things as they are now and moving forward from wherever I'm standing.

## Disclaimer

I am not a professional doctor or a therapist. I'm not qualified to give you medical advice. I'm a programmer. All of the information in this book is given from the perspective of a struggling programmer and should not be taken as medical advice. If you need help from a medical professional please seek out that help (There is an entire chapter about seeking help from others near the end of this book).

Let's begin our journey by figuring out where we are and remembering what lead us to this place.
