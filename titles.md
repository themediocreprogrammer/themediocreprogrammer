# The Mediocre Programmer


A handbook for growing the programmer within.
A love-letter to an industry and those struggling to make a go of it.
