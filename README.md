This is the (work-in-progress) text for the upcoming book "The Mediocre Programmer".

This book is mostly complete, but I still need to do more editing. Also chapter 7 still needs some work based on feedback.

Released under a CC-BY-SA 4.0 International license.

(Uses Pandoc Markdown Template / Makefile from https://github.com/evangoer/pandoc-ebook-template)
