# Gratitude

This book would not exist without the folks who have accompanied me on my journey, both as instructors and as colleagues. My thanks and appreciation to all of my instructors in my formative years for giving me their best efforts to teach me programming in its various forms. I am indebted to all of my colleagues and programming friends over the years who shared their knowledge with me and trusted me enough to help them along the way. I am also blessed to have many communities that help sustain me, including the [Michigan!/usr/group][1], [PyOhio][2], [Coffee House Coders][3], and the [Ubuntu Michigan Loco][4]. Also to those who don't fit in these neat categories; know that if we have spent any time discussing programming or other matters that our discussions are deeply appreciated.

I also am grateful for the work of Leo Babauta of [Zen Habits][5] which provided me the ideas of mindfulness and focus containers. They have been transformative in my own work, as this book demonstrates. I committed to spending at least 10 minutes each morning writing each section, and the results are the work you see before you.

Thank you to those who helped me directly with this project. Thank you to my mom, Sharon Maloney, for help in my editing  of this book. Any mist steaks what remain are an responsibilities of the author. Thank you to Beau Sheldon for reviewing the chapter on mental health and for helping me to better understand and highlight areas where folks struggle. Thank you to my friend, David Revoy, for his amazing cover art and for his inspiration throughout the project. Thank you to Esteban Manchado Velázquez for adding CSS and cleaning up the HTML version of the text. Thank you to the beta readers for your valuable comments and feedback in the Framagit Repository, including (in alphabetical order by handle or first name): Brendan Kidwell, D. Joe Anderson, David Revoy, Eric Hallam, Jer Lance, Matthew Piccinato, Matthew Balch, Midgard, Nicholas Guarracino, RJ Quiralta, Valvin, and Wilhelm Fitzpatrick. Thank you to Paco Esteban for editing fixes.

My deepest gratitude goes to my wife JoDee and my parents for their support and belief in me. Words cannot express the love and thanks I have for you.

[1]:http://mug.org
[2]:https://pyohio.org
[3]:http://coffeehousecoders.com
[4]:htttp://loco.ubuntu.com/teams/ubuntu-us-mi
[5]:http://zenhabits.net
